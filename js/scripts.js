if (navigator.geolocation) {
  // Request the current position
  // If successful, call getWeather; if not call getError
  navigator.geolocation.getCurrentPosition(getWeather, getError);
} else {
  alert('Geolocation is not available!');
}


function getWeather(pos) {
  // Get the coordinates of the current possition.
  let geoLatitude = String(pos.coords.latitude.toFixed(5));
  let geoLongitude = String(pos.coords.longitude.toFixed(5));
  console.log(pos.coords);

  //construct the url and call httpRequestAsync
  let url = "https://fcc-weather-api.glitch.me/api/current?lat=" + geoLatitude + "&lon=" + geoLongitude;
  //var url =  "api.openweathermap.org/data/2.5/weather?lat=" + geoLatitude + "&lon=" + geoLongitude + "&appid=469d750ad029db401d0afa966f723634";
  httpRequestAsync(url, showWeather);
}


// getCurrentPosition: Error returned
function getError(err) {
  switch (err.code) {
    case err.PERMISSION_DENIED:
      alert("User denied the request for Geolocation.");
      break;
    case err.POSITION_UNAVAILABLE:
      alert("Location information is unavailable.");
      break;
    case err.TIMEOUT:
      alert("The request to get user location timed out.");
      break;
    default:
      alert("An unknown error occurred.");
  }
}


function httpRequestAsync(url, callback) {
  let httpRequest = new XMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4 && httpRequest.status == 200)
      callback(httpRequest.responseText);
  }
  httpRequest.open("GET", url, true); // true for asynchronous 
  httpRequest.send();
}

function showWeather(response) {
  let cityName = document.getElementById("city-name");
  let textWeather = document.getElementById("text-weather");
  let textCelsius = document.getElementById("text-celsius");
  let textWind = document.getElementById("text-wind");
  let jsonObject = JSON.parse(response);
  let weatherIcon = document.getElementById("weather-icon");
  let backgroundSource = document.getElementById("image-weather");

  console.log(jsonObject);

  cityName.innerHTML = jsonObject.name;
  textWeather.innerHTML = jsonObject.weather[0].description;
  textCelsius.innerHTML = String(jsonObject.main.temp.toFixed(0)) + " °C";
  textWind.innerHTML = jsonObject.wind.speed + " km/h";

  //zum testen
  //jsonObject.weather[0].main = "clear";
  //jsonObject.weather[0].main = "clouds";
  //jsonObject.weather[0].main = "rain";
  //jsonObject.weather[0].main = "thunderstorm";
  //jsonObject.weather[0].main = "snow";
  //jsonObject.weather[0].main = "mist";

  switch (jsonObject.weather[0].main) {

    case "Clear":
      weatherIcon.className = "wi wi-day-sunny";
      backgroundSource.src = "img/sunny.png";
      document.getElementById("main").style.backgroundColor = "rgb(237, 123, 27)";
      cityName.style.color = "rgb(237, 123, 27)";
      break;

    case "Clouds":
      weatherIcon.className = "wi wi-cloud";
      backgroundSource.src = "img/cloudy.png";
      document.getElementById("main").style.backgroundColor = "rgb(0, 153, 61)";
      cityName.style.color = "rgb(0, 153, 61)";

      break;

    case "Rain":
      weatherIcon.className = "wi wi-showers";
      backgroundSource.src = "img/rainy.png";
      document.getElementById("main").style.backgroundColor = "rgb(65, 71, 71)";
      cityName.style.color = "rgb(65, 71, 71)";

      break;

    case "Drizzle":
      weatherIcon.className = "wi wi-showers";
      backgroundSource.src = "img/rainy.png";
      document.getElementById("main").style.backgroundColor = "rgb(65, 71, 71)";
      cityName.style.color = "rgb(65, 71, 71)";

      break;

    case "Thunderstorm":
      weatherIcon.className = "wi wi-thunderstorm";
      backgroundSource.src = "img/thunderstorm.png";
      document.getElementById("main").style.backgroundColor = "rgb(65, 71, 71)";
      cityName.style.color = "rgb(65, 71, 71)";

      break;

    case "Snow":
      weatherIcon.className = "wi wi-snow";
      backgroundSource.src = "img/snowy.png";
      document.getElementById("main").style.backgroundColor = "rgb(163, 61, 95)";
      cityName.style.color = "rgb(163, 61, 95)";

      break;

    case "Mist":
      weatherIcon.className = "wi wi-fog";
      backgroundSource.src = "img/foggy.png";
      document.getElementById("main").style.backgroundColor = "rgb(158, 161, 162)";
      cityName.style.color = "rgb(158, 161, 162)";

      break;

    default:
      weatherIcon.className = "wi wi-day-cloudy";
      backgroundSource.src = "img/cloudy.png";
      document.getElementById("main").style.backgroundColor = "rgb(0, 153, 61)";
      cityName.style.color = "rgb(0, 153, 61)";

      break;

  }
  wrapper = document.getElementById("wrapper");
  loader = document.getElementById("lds-ellipsis");

  loader.style.opacity = 0;
  wrapper.style.opacity = 1;
}

